#+title: Jack Stephenson's Manual
#+author: Jack Stephenson

* Who am I?

Hey, I'm Jack Stephenson, though I typically just go by Jack.

My pronouns of choice are They/He.

I'm located in London, United Kingdom, and operate in Greenwich Mean Time (GMT), tending later.

** Background

I have been born and raised in London, in the United Kingdom, with a brief break in Sheffield
for University.

My journey so far has included a Master's degree in Computer Science
and Artificial Intelligence (focusing on reinforcement learning),
interfacing with electronics and scientific equipment.  From this I
then moved from a full stack engineering role through a developer
experience role to a site reliability engineering role.

# TODO: ** Values

** Expectations

*** from my manager

- Direct and actionable feedback in our 1:1s so I can improve.
- Guidance on organizational dynamics and useful places to direct my effort.
- Context for how my team's work fits into the bigger picture.
- Clear, documented expectations.
- Trust and empowerment to do the job I'm here for.

*** from my peers

- Direct and actionable feedback about how my actions impact them so I can improve.
- Partnership and collaboration.

*** from my employer

- Immediate and direct communication about where we are and where we want to go.
- Acknowledgement of areas of growth, both personally and for the department/company.
- Leading by example.
- Sincerely inviting feedback.

** Schedule

- I work in the GMT time zone.
- I am not a morning person, but I'm happy to try and make any
  reasonable time work when needed.

# TODO: ** Communication

# TODO: ** Style

** Challenges

** Hobbies

- Photography.
- Building software & experimenting in my homelab.
- Travelling to see new places.
- Learning Portuguese (pt-PT).

*** Aspirational hobbies

These are things that I have enjoyed in the past and want to return
to, but don't currently engage in.

- Mountain biking
- Bouldering
- Inline Skating
